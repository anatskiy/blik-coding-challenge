# Blik Coding Challenge

## Description

The task was to solve the challenge using React and Redux. Not much choice here.

In order to quickly build a UI, I chose [Material UI](https://material-ui-next.com/). It contains tons of ready-to-use React components. The application is fully responsive and looks nice both on a desktop and mobile devices.

As the challenge required some visualization, I compared a few chart libraries (Chart.js, Chartist, plotly.js) and decided to stick with Chart.js because it is lightweight, easy to customize and has a React wrapper (actually, all of them have).

`axios` is used to send requests to the server. I could go with the native `fetch API`, but `axios` is just simpler to use and can effortlessly cancel the sent requests (I do it to prevent memory leaks when the app unmounts components before a request completion).

I structured the project in the following way:

- `src/config.js` contains common constants like backend URL and Google Maps API key
- `src/routes.js` contains all the project's routes (only one of them is the actual route, Analytics, others just redirect to it)
- `src/actionTypes.js`, `src/actions.js`, and `src/reducers.js` are Redux-related files. The last two files can be splitted into separate files as the app grows.
- `src/containers` contains stateful components, which fetch data from the server and interact with Redux
- `src/components` contains (mostly) presentational components, which just show the data, provided by corresponding container components
	- such organization allowed a better separation of concerns and reusability
	
## Further steps

The next step would be to write tests. Tests are crucial to ensure everything works as expected regardless of the input or use cases. I'd test the following:

- Components
	- Build Verification Testing - basic tests to make sure each component renders properly
	- Verify props and its types and shapes (for arrays and objects)
- Redux
	- Reducers - provide some input data and verify the output
	- Action creators - test action's type and expected values
- API calls
	-  All interactions with the backend or external services should be mocked

As for the frameworks choice, I'd pick Jest and Enzyme.

## How to run

1. Setup the backend server as described [here](https://gitlab.com/publik/code-challenges/mock-server)
2. Change the server URL in `src/config.js` if needed
3. Install the requirements with `yarn install`
4. Run the frontend server with `yarn start`

## Other

Time spent on the challenge: `3.5 days`
