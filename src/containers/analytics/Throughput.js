import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import axios from 'axios';
import moment from 'moment';
import Noty from 'noty';
import { API_ENDPOINT } from 'config';
import { fetchThroughput } from 'actions';
import Loader from 'components/Loader';
import Throughput from 'components/analytics/Throughput';
import ErrorMessage from 'components/analytics/ErrorMessage';

const mapStateToProps = state => ({
  throughput: state.analyticsDetail.throughput
});

const mapDispatchToProps = dispatch => ({
  fetchThroughput: payload => dispatch(fetchThroughput(payload))
});

class ThroughputContainer extends Component {
  constructor () {
    super();
    this.state = {
      error: '',
      isLoading: true
    };
  }

  processData (data) {
    const max = [];
    const avg = [];
    const min = [];

    data.forEach(obj => {
      let date = new Date(obj.timestamp);
      max.push({ x: date, y: obj.max });
      avg.push({ x: date, y: obj.avg });
      min.push({ x: date, y: obj.min });
    });

    return { maxThroughput: max, avgThroughput: avg, minThroughput: min };
  }

  getChartData (data) {
    const { maxThroughput, avgThroughput, minThroughput } = this.processData(data);
    return {
      datasets: [
        {
          label: 'Maximal',
          data: maxThroughput,
          fill: false,
          backgroundColor: '#ff6f00', // 900
          borderColor: '#ff6f00'      // 900
        },
        {
          label: 'Durchschnittlich',
          data: avgThroughput,
          fill: false,
          backgroundColor: '#ffc107', // 500
          borderColor: '#ffc107'      // 500
        },
        {
          label: 'Minimal',
          data: minThroughput,
          fill: false,
          backgroundColor: '#ffecb3', // 100
          borderColor: '#ffecb3'      // 100
        }
      ]
    };
  }

  getChartOptions () {
    return {
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [{
          type: 'time',
          time: {
            unit: 'day',
            displayFormats: {
              day: 'MMMM D'
            }
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      },
      tooltips: {
        callbacks: {
          title: (tooltipItem, data) => {
            const { index, datasetIndex } = tooltipItem[0];
            const date = data.datasets[datasetIndex].data[index].x;
            return moment(date).format('LL');
          }
        }
      }
    };
  }

  fetchThroughput (id) {
    axios.get(`${API_ENDPOINT}/analytics/${id}/throughput`, {
      cancelToken: this.source.token
    }).then(response => {
      this.props.fetchThroughput(response.data);
      this.setState({ isLoading: false });
    }).catch(error => {
      if (error.toString() !== 'Cancel') {
        this.setState({ error: error.message });
        new Noty({ text: error.message, type: 'error' }).show();
        console.log(error.message);
      }
    });
  }

  componentDidMount () {
    this.source = axios.CancelToken.source();
    this.fetchThroughput(this.props.id);
  }

  componentWillUnmount () {
    // Abort the request if the component will unmount before request completion
    this.source.cancel();
  }

  render () {
    const { error, isLoading } = this.state;
    const { throughput } = this.props;

    return <Fragment>
      {isLoading && error === '' && <Loader margin={50} />}
      {error !== '' && <ErrorMessage />}
      {!isLoading && error === '' &&
        <Throughput
          chartData={this.getChartData(throughput)}
          chartOptions={this.getChartOptions()}
        />
      }
    </Fragment>;
  }
}

ThroughputContainer.propTypes = {
  id: PropTypes.string.isRequired,
  fetchThroughput: PropTypes.func.isRequired,
  throughput: PropTypes.arrayOf(
    PropTypes.shape({
      timestamp: PropTypes.number.isRequired,
      max: PropTypes.number.isRequired,
      avg: PropTypes.number.isRequired,
      min: PropTypes.number.isRequired
    }).isRequired
  ).isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(ThroughputContainer);
