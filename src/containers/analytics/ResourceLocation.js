import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ResourceLocation from 'components/analytics/ResourceLocation';

const mapStateToProps = state => ({
  location: state.analyticsDetail.location
});

const ResourceLocationContainer = ({ location }) =>
  <ResourceLocation location={location} />;

ResourceLocation.propTypes = {
  location: PropTypes.shape({
    lat: PropTypes.number.isRequired,
    lng: PropTypes.number.isRequired
  }).isRequired
};

export default connect(mapStateToProps)(ResourceLocationContainer);
