import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
import Zones from 'components/analytics/Zones';

const mapStateToProps = state => ({
  zones: state.analyticsDetail.zones
});

class ZonesContainer extends Component {
  constructor () {
    super();
    this.state = {
      zone: 0,
      zoneDescription: '',
      zoneChartData: []
    };
  }

  processData (data) {
    return data.map(obj => {
      return { x: new Date(obj.timestamp), y: obj.value };
    });
  }

  selectZone (event) {
    const zone = event.target.value;
    const zoneObj = this.props.zones[zone];

    if (this.state.zone === zone) return;

    this.setState({
      zone: zone,
      zoneDescription: zoneObj.description,
      zoneChartData: this.processData(zoneObj.lengthOfStay)
    });
  }

  getChartData () {
    return {
      datasets: [{
        data: this.state.zoneChartData,
        backgroundColor: '#ffc107', // 500
        borderColor: '#ffc107',     // 500
        fill: false
      }]
    };
  }

  getChartOptions () {
    return {
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          type: 'time',
          time: {
            unit: 'day',
            displayFormats: {
              day: 'MMMM D'
            }
          }
        }]
      },
      tooltips: {
        callbacks: {
          title: (tooltipItem, data) => {
            const { index, datasetIndex } = tooltipItem[0];
            const date = data.datasets[datasetIndex].data[index].x;
            return moment(date).format('LL');
          }
        }
      }
    };
  }

  componentWillMount () {
    const zones = this.props.zones;
    if (zones.length > 0) {
      this.setState({
        zoneDescription: zones[0].description,
        zoneChartData: this.processData(zones[0].lengthOfStay)
      });
    }
  }

  render () {
    const { zones } = this.props;
    const { zone, zoneDescription } = this.state;

    return <Zones
      zoneNames={zones.map(z => z.name)}
      selectZone={this.selectZone.bind(this)}
      zone={zone}
      zoneDescription={zoneDescription}
      chartData={this.getChartData()}
      chartOptions={this.getChartOptions()}
    />;
  }
}

ZonesContainer.propTypes = {
  zones: PropTypes.arrayOf(
    PropTypes.shape({
      description: PropTypes.string.isRequired,
      lengthOfStay: PropTypes.arrayOf(
        PropTypes.shape({
          timestamp: PropTypes.number.isRequired,
          value: PropTypes.number.isRequired
        }).isRequired
      ).isRequired
    }).isRequired
  ).isRequired
};

export default connect(mapStateToProps)(ZonesContainer);
