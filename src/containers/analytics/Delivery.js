import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import axios from 'axios';
import moment from 'moment';
import Noty from 'noty';
import { API_ENDPOINT } from 'config';
import { fetchDeliveries } from 'actions';
import Loader from 'components/Loader';
import Delivery from 'components/analytics/Delivery';
import ErrorMessage from 'components/analytics/ErrorMessage';

const timeFormats = {
  day: 'MMMM D',
  week: '[KW] w',
  month: 'MMMM YYYY'
};

const mapStateToProps = state => ({
  deliveries: state.analyticsDetail.deliveries
});

const mapDispatchToProps = dispatch => ({
  fetchDeliveries: payload => dispatch(fetchDeliveries(payload))
});

class DeliveryContainer extends Component {
  constructor () {
    super();
    this.state = {
      error: '',
      isLoading: true,
      deliveryAggregateBy: 'day'
    };
  }

  getChartData (data) {
    return {
      datasets: [{
        data: data.map(d => {
          return { x: new Date(d.timestamp), y: d.value };
        }),
        fill: false,
        backgroundColor: '#ffc107', // 500
        borderColor: '#ffc107'      // 500
      }]
    };
  }

  getChartOptions () {
    return {
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          type: 'time',
          time: {
            unit: this.state.deliveryAggregateBy,  // day, month, week
            displayFormats: timeFormats
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      },
      tooltips: {
        callbacks: {
          title: (tooltipItem, data) => {
            const { index, datasetIndex } = tooltipItem[0];
            const date = data.datasets[datasetIndex].data[index].x;
            return moment(date).format('LL');
          }
        }
      }
    };
  }

  handleDeliveryAggChange (event) {
    const id = this.props.id;
    const deliveryAggregateBy = event.target.value;
    if (deliveryAggregateBy === this.state.deliveryAggregateBy) return;
    this.fetchDeliveries(id, deliveryAggregateBy);
  }

  fetchDeliveries (id, deliveryAggregateBy) {
    let url = `${API_ENDPOINT}/analytics/${id}/delivery`;
    deliveryAggregateBy = deliveryAggregateBy || 'day';

    if (deliveryAggregateBy === 'week' || deliveryAggregateBy === 'month') {
      url += '?' + deliveryAggregateBy;
    }

    axios.get(url, {
      cancelToken: this.source.token
    }).then(response => {
      this.props.fetchDeliveries(response.data);
      this.setState({
        isLoading: false,
        deliveryAggregateBy: deliveryAggregateBy
      });
    }).catch(error => {
      if (error.toString() !== 'Cancel') {
        this.setState({ error: error.message });
        new Noty({ text: error.message, type: 'error' }).show();
        console.log(error.message);
      }
    });
  }

  componentDidMount () {
    this.source = axios.CancelToken.source();
    this.fetchDeliveries(this.props.id);
  }

  componentWillUnmount () {
    // Abort the request if the component will unmount before request completion
    this.source.cancel();
  }

  render () {
    const { deliveries } = this.props;
    const { error, isLoading, deliveryAggregateBy } = this.state;

    return <Fragment>
      {isLoading && error === '' && <Loader margin={50} />}
      {error !== '' && <ErrorMessage />}
      {!isLoading && error === '' &&
        <Delivery
          deliveryAggregateBy={deliveryAggregateBy}
          handleDeliveryAggChange={this.handleDeliveryAggChange.bind(this)}
          chartData={this.getChartData(deliveries)}
          chartOptions={this.getChartOptions()}
        />
      }
    </Fragment>;
  }
}

DeliveryContainer.propTypes = {
  id: PropTypes.string.isRequired,
  fetchDeliveries: PropTypes.func.isRequired,
  deliveries: PropTypes.arrayOf(
    PropTypes.shape({
      timestamp: PropTypes.number.isRequired,
      value: PropTypes.number.isRequired
    }).isRequired
  ).isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(DeliveryContainer);
