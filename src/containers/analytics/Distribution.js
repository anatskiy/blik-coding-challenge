import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import axios from 'axios';
import moment from 'moment';
import Noty from 'noty';
import { API_ENDPOINT } from 'config';
import { fetchDistribution } from 'actions';
import Loader from 'components/Loader';
import Distribution from 'components/analytics/Distribution';
import ErrorMessage from 'components/analytics/ErrorMessage';

const mapStateToProps = state => ({
  distribution: state.analyticsDetail.distribution
});

const mapDispatchToProps = dispatch => ({
  fetchDistribution: payload => dispatch(fetchDistribution(payload))
});

class DistributionContainer extends Component {
  constructor () {
    super();
    this.state = {
      error: '',
      isLoading: true,
      distributionFilter: 'all'
    };
  }

  processData (data) {
    const distributionFilter = this.state.distributionFilter;
    const consumer = [];
    const producer = [];
    const transit = [];

    data.forEach(obj => {
      let consumerValue;
      let producerValue;
      let transitValue;

      if (distributionFilter === 'empty') {
        consumerValue = obj.value.consumer.empty;
        producerValue = obj.value.producer.empty;
        transitValue = obj.value.transit.empty;
      } else if (distributionFilter === 'full') {
        consumerValue = obj.value.consumer.full;
        producerValue = obj.value.producer.full;
        transitValue = obj.value.transit.full;
      } else {
        consumerValue = obj.value.consumer.empty + obj.value.consumer.full;
        producerValue = obj.value.producer.empty + obj.value.producer.full;
        transitValue = obj.value.transit.empty + obj.value.transit.full;
      }

      consumer.push(consumerValue);
      producer.push(producerValue);
      transit.push(transitValue);
    });

    return {
      consumerData: consumer,
      producerData: producer,
      transitData: transit
    };
  }

  getChartData (data) {
    const { consumerData, producerData, transitData } = this.processData(data);
    const labels = data.map(obj => new Date(obj.timestamp));

    return {
      labels: labels,
      datasets: [
        {
          label: 'Verbraucher',
          data: consumerData,
          backgroundColor: '#ffa000' // 700
        },
        {
          label: 'Produzent',
          data: producerData,
          backgroundColor: '#ffca28' // 400
        },
        {
          label: 'In Transit',
          data: transitData,
          backgroundColor: '#ffecb3' // 100
        }
      ]
    };
  }

  getChartOptions () {
    return {
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [{
          stacked: true,
          type: 'time',
          time: {
            unit: 'day',
            displayFormats: {
              day: 'MMMM D'
            }
          }
        }],
        yAxes: [{
          stacked: true
        }]
      },
      tooltips: {
        mode: 'index',
        intersect: false,
        callbacks: {
          title: (tooltipItem, data) => {
            const date = data.labels[tooltipItem[0].index];
            return moment(date).format('LL');
          }
        }
      }
    };
  }

  changeDistributionFilter (event) {
    this.setState({ distributionFilter: event.target.value });
  }

  fetchDistribution (id) {
    axios.get(`${API_ENDPOINT}/analytics/${id}/distribution`, {
      cancelToken: this.source.token
    }).then(response => {
      this.props.fetchDistribution(response.data);
      this.setState({ isLoading: false });
    }).catch(error => {
      if (error.toString() !== 'Cancel') {
        this.setState({ error: error.message });
        new Noty({ text: error.message, type: 'error' }).show();
        console.log(error.message);
      }
    });
  }

  componentDidMount () {
    this.source = axios.CancelToken.source();
    this.fetchDistribution(this.props.id);
  }

  componentWillUnmount () {
    // Abort the request if the component will unmount before request completion
    this.source.cancel();
  }

  render () {
    const { distribution } = this.props;
    const { error, isLoading, distributionFilter } = this.state;

    return <Fragment>
      {isLoading && error === '' && <Loader margin={50} />}
      {error !== '' && <ErrorMessage />}
      {!isLoading && error === '' &&
        <Distribution
          chartData={this.getChartData(distribution)}
          chartOptions={this.getChartOptions()}
          distributionFilter={distributionFilter}
          changeDistributionFilter={this.changeDistributionFilter.bind(this)}
        />
      }
    </Fragment>;
  }
}

DistributionContainer.propTypes = {
  id: PropTypes.string.isRequired,
  fetchDistribution: PropTypes.func.isRequired,
  distribution: PropTypes.arrayOf(
    PropTypes.shape({
      timestamp: PropTypes.number.isRequired,
      value: PropTypes.shape({
        consumer: PropTypes.shape({
          empty: PropTypes.number.isRequired,
          full: PropTypes.number.isRequired
        }).isRequired,
        producer: PropTypes.shape({
          empty: PropTypes.number.isRequired,
          full: PropTypes.number.isRequired
        }).isRequired,
        transit: PropTypes.shape({
          empty: PropTypes.number.isRequired,
          full: PropTypes.number.isRequired
        }).isRequired
      }).isRequired
    }).isRequired
  ).isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(DistributionContainer);
