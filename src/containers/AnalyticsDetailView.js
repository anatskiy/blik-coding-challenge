import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import axios from 'axios';
import 'moment/locale/de';
import { API_ENDPOINT } from '../config';
import { fetchResource } from '../actions';
import AnalyticsDetail from 'components/AnalyticsDetail';

const mapStateToProps = state => ({
  id: state.analyticsDetail.id,
  name: state.analyticsDetail.name,
  numberOfSLTs: state.analyticsDetail.numberOfSLTs
});

const mapDispatchToProps = dispatch => ({
  fetchResource: payload => dispatch(fetchResource(payload))
});

class AnalyticsDetailView extends Component {
  constructor () {
    super();
    this.state = {
      isLoading: true
    };
  }

  fetchResource (id) {
    axios.get(`${API_ENDPOINT}/analytics/${id}`)
      .then(response => {
        if (!response.data.hasOwnProperty('id')) {
          this.props.history.push('/analytics');
          return;
        }
        this.props.fetchResource(response.data);
        this.setState({ isLoading: false });
      })
      .catch(error => {
        console.log(error);
        // Redirect to Analytics page
        this.props.history.push('/analytics');
      });
  }

  componentDidMount () {
    const id = this.props.match.params.id;
    this.fetchResource(id);
  }

  render () {
    const id = this.props.match.params.id;
    const { isLoading } = this.state;
    const { name, numberOfSLTs } = this.props;

    return <AnalyticsDetail
      id={id}
      name={name}
      isLoading={isLoading}
      numberOfSLTs={numberOfSLTs}
    />;
  }
}

AnalyticsDetailView.propTypes = {
  name: PropTypes.string.isRequired,
  numberOfSLTs: PropTypes.number.isRequired,
  fetchResource: PropTypes.func.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(AnalyticsDetailView);
