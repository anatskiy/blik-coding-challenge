import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import axios from 'axios';
import Noty from 'noty';
import { API_ENDPOINT } from '../config';
import { fetchResourceIds } from '../actions';
import AnalyticsList from 'components/AnalyticsList';

const mapStateToProps = state => ({
  ids: state.analytics.ids
});

const mapDispatchToProps = dispatch => ({
  fetchResourceIds: ids => dispatch(fetchResourceIds(ids))
});

class AnalyticsListView extends Component {
  constructor () {
    super();
    this.state = {
      isLoading: true
    };
  }

  componentDidMount () {
    axios.get(`${API_ENDPOINT}/analytics`)
      .then(response => {
        this.props.fetchResourceIds(response.data);
        this.setState({ isLoading: false });
      })
      .catch(error => {
        this.setState({ isLoading: false });
        new Noty({ text: error.message, type: 'error' }).show();
        console.log(error);
      });
  }

  render () {
    return <AnalyticsList
      ids={this.props.ids}
      isLoading={this.state.isLoading}
    />;
  }
}

AnalyticsListView.propTypes = {
  ids: PropTypes.arrayOf(
    PropTypes.string.isRequired
  ).isRequired,
  fetchResourceIds: PropTypes.func.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(AnalyticsListView);
