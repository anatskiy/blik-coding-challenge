import React from 'react';
import { CircularProgress } from 'material-ui/Progress';

const Loader = ({ margin, size }) =>
  <div style={{ textAlign: 'center', margin: margin }}>
    <CircularProgress size={size || 50} />
  </div>;

export default Loader;
