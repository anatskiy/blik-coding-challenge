import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';

import DeliveryCard from 'containers/analytics/Delivery';
import ThroughputCard from 'containers/analytics/Throughput';
import DistributionCard from 'containers/analytics/Distribution';
import ZonesCard from 'containers/analytics/Zones';
import ResourceLocationCard from 'containers/analytics/ResourceLocation';

const styles = theme => ({
  resourceName: {
    [theme.breakpoints.down('xs')]: {
      fontSize: `calc(${theme.typography.display1.fontSize} * 0.75)`
    }
  }
});

const AnalyticsDetail = props => {
  const { classes, id, name, numberOfSLTs, isLoading } = props;

  return (
    <Grid container spacing={24}>
      <Grid item xs={12}>
        <Typography variant='display1' className={classes.resourceName}>
          {name}
        </Typography>
        <Typography variant='subheading'>
          Gesamtzahl SLTs im Kreislauf: {numberOfSLTs}
        </Typography>
      </Grid>
      <Grid item xs={12} lg={6}>
        {!isLoading && <DeliveryCard id={id} />}
      </Grid>
      <Grid item xs={12} lg={6}>
        {!isLoading && <ThroughputCard id={id} />}
      </Grid>
      <Grid item xs={12}>
        {!isLoading && <DistributionCard id={id} />}
      </Grid>
      <Grid item xs={12} lg={6}>
        {!isLoading && <ZonesCard />}
      </Grid>
      <Grid item xs={12} lg={6}>
        {!isLoading && <ResourceLocationCard />}
      </Grid>
    </Grid>
  );
};

AnalyticsDetail.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
  numberOfSLTs: PropTypes.number.isRequired,
  isLoading: PropTypes.bool.isRequired
};

export default withStyles(styles)(AnalyticsDetail);
