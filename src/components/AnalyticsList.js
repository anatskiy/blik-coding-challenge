import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Table, {
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from 'material-ui/Table';
import Loader from 'components/Loader';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit,
    overflowX: 'auto'
  },
  loader: {
    margin: theme.spacing.unit
  },
  link: {
    color: 'inherit',
    textDecoration: 'none',
    '&:hover': {
      color: '#757575',
      textDecoration: 'underline'
    },
    '&:focus': {
      color: '#9e9e9e'
    }
  }
});

const EmptyRow = ({ component }) =>
  <TableRow>
    <TableCell colSpan={2} style={{ textAlign: 'center' }}>
      {component}
    </TableCell>
  </TableRow>;

const Analytics = ({ ids, classes, isLoading }) =>
  <Grid container justify='center'>
    <Grid item xs={12} sm={8} md={8} lg={6} xl={4}>
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell style={{ width: 15 }} />
              <TableCell>Analytics Resource ID</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {isLoading && <EmptyRow component={<Loader margin={25} />} />}
            {!isLoading && ids.length === 0 && <EmptyRow component={'No data.'} />}
            {ids.map((id, idx) =>
              <TableRow hover key={id}>
                <TableCell numeric>{idx + 1}</TableCell>
                <TableCell>
                  <NavLink to={`analytics/${id}`} className={classes.link}>
                    {id}
                  </NavLink>
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </Paper>
    </Grid>
  </Grid>;

Analytics.propTypes = {
  ids: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired
};

export default withStyles(styles)(Analytics);
