import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';

import { toggleSidebar } from '../actions';

const styles = theme => ({
  appBar: {
    position: 'absolute',
    marginLeft: theme.drawerWidth,
    [theme.breakpoints.up('md')]: {
      width: `calc(100% - ${theme.drawerWidth}px)`
    }
  },
  navIcon: {
    color: theme.palette.common.white,
    [theme.breakpoints.up('md')]: {
      display: 'none'
    }
  },
  accIcon: {
    color: theme.palette.common.white
  },
  flex: {
    flex: 1
  },
  textFieldRoot: {
    padding: 0
  },
  textFieldInput: {
    borderRadius: 3,
    backgroundColor: theme.palette.primary[300],
    color: '#5d4037',
    fontSize: 16,
    padding: '10px 12px',
    marginRight: 15,
    [theme.breakpoints.down('sm')]: {
      marginLeft: 15
    },
    '&:focus': {
      backgroundColor: theme.palette.primary[200]
    },
    '&:hover': {
      backgroundColor: theme.palette.primary[200]
    }
  }
});

const mapDispatchToProps = dispatch => ({
  toggleSidebar: () => dispatch(toggleSidebar())
});

const Header = ({ classes, toggleSidebar }) =>
  <AppBar className={classes.appBar}>
    <Toolbar>
      <IconButton
        color='inherit'
        aria-label='open drawer'
        onClick={() => { toggleSidebar(); }}
        className={classes.navIcon}
      >
        <MenuIcon />
      </IconButton>
      <TextField
        fullWidth
        placeholder='Search...'
        InputProps={{
          disableUnderline: true,
          classes: {
            root: classes.textFieldRoot,
            input: classes.textFieldInput
          }
        }}
      />
      <IconButton color='inherit' className={classes.accIcon}>
        <AccountCircle />
      </IconButton>
    </Toolbar>
  </AppBar>;

Header.propTypes = {
  classes: PropTypes.object.isRequired,
  toggleSidebar: PropTypes.func.isRequired
};

export default withStyles(styles)(
  connect(null, mapDispatchToProps)(Header)
);
