const styles = theme => ({
  cardTitle: {
    [theme.breakpoints.down('xs')]: {
      fontSize: `calc(${theme.typography.headline.fontSize} * 0.65)`
    }
  },
  cardContent: {
    padding: 10,
    '&:last-child': {
      paddingBottom: 5
    },
    '& canvas': {
      width: '100% !important',
      height: '200px !important'
    }
  },
  throughputCard: {
    '& canvas': {
      height: '208px !important'
    }
  },
  deliverySelect: {
    margin: theme.spacing.unit,
    minWidth: 100
  },
  radioGroup: {
    flexDirection: 'row',
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column'
    }
  },
  distributionFilter: {
    [theme.breakpoints.down('xs')]: {
      height: 28
    }
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  }
});

export default styles;
