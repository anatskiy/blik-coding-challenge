import React from 'react';
import PropTypes from 'prop-types';
import { Bar } from 'react-chartjs-2';

import { withStyles } from 'material-ui/styles';
import Card, { CardHeader, CardContent } from 'material-ui/Card';
import { FormControlLabel } from 'material-ui/Form';
import Radio, { RadioGroup } from 'material-ui/Radio';
import CardStyles from './CardStyles';

const Distribution = props =>
  <Card>
    <CardHeader
      title={'Verteilung der SLTs im Kreislauf'}
      classes={{title: props.classes.cardTitle}}
      action={
        <RadioGroup
          value={props.distributionFilter}
          onChange={e => props.changeDistributionFilter(e)}
          className={props.classes.radioGroup}
        >
          <FormControlLabel
            value='all'
            label='Alle SLTs'
            control={<Radio className={props.classes.distributionFilter} />}
          />
          <FormControlLabel
            value='empty'
            label='Leergut'
            control={<Radio className={props.classes.distributionFilter} />}
          />
          <FormControlLabel
            value='full'
            label='Vollgut'
            control={<Radio className={props.classes.distributionFilter} />}
          />
        </RadioGroup>
      }
    />
    <CardContent className={props.classes.cardContent}>
      <Bar data={props.chartData} options={props.chartOptions} />
    </CardContent>
  </Card>;

Distribution.propTypes = {
  classes: PropTypes.object.isRequired,
  chartData: PropTypes.object.isRequired,
  chartOptions: PropTypes.object.isRequired,
  distributionFilter: PropTypes.string.isRequired,
  changeDistributionFilter: PropTypes.func.isRequired
};

export default withStyles(CardStyles)(Distribution);
