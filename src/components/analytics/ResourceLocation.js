import React from 'react';
import PropTypes from 'prop-types';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps';
import { withStyles } from 'material-ui/styles';
import Card, { CardContent } from 'material-ui/Card';
import { MAPS_API_KEY } from 'config';

const resourceMapCardHeight = 352;

const styles = theme => ({
  resourceMapCard: {
    height: resourceMapCardHeight,
    overflow: 'hidden'
  },
  resourceMapCardContent: {
    padding: 0,
    '&:last-child': {
      padding: 0
    }
  },
  mapContainerElement: {
    height: resourceMapCardHeight
  }
});

const MapComponent = withScriptjs(withGoogleMap(props =>
  <GoogleMap defaultZoom={15} defaultCenter={{...props.location}}>
    <Marker position={{...props.location}} />
  </GoogleMap>
));

const ResourceLocation = ({ classes, location }) =>
  <Card className={classes.resourceMapCard}>
    <CardContent className={classes.resourceMapCardContent}>
      <MapComponent
        location={location}
        googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${MAPS_API_KEY}`}
        loadingElement={<div style={{ height: '100%' }} />}
        containerElement={<div className={classes.mapContainerElement} />}
        mapElement={<div style={{ height: '100%' }} />}
      />
    </CardContent>
  </Card>;

ResourceLocation.propTypes = {
  classes: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired
};

export default withStyles(styles)(ResourceLocation);
