import React from 'react';
import PropTypes from 'prop-types';
import { Line } from 'react-chartjs-2';

import { withStyles } from 'material-ui/styles';
import Card, { CardHeader, CardContent } from 'material-ui/Card';
import Select from 'material-ui/Select';
import { MenuItem } from 'material-ui/Menu';
import CardStyles from './CardStyles';

const Delivery = props =>
  <Card>
    <CardHeader
      title={'Anzahl der Lieferungen'}
      classes={{title: props.classes.cardTitle}}
      action={
        <Select
          className={props.classes.deliverySelect}
          value={props.deliveryAggregateBy}
          onChange={e => props.handleDeliveryAggChange(e)}
          inputProps={{ name: 'deliveryAggregateBy' }}
        >
          <MenuItem value={'day'}>Tag</MenuItem>
          <MenuItem value={'week'}>Woche</MenuItem>
          <MenuItem value={'month'}>Monat</MenuItem>
        </Select>
      }
    />
    <CardContent className={props.classes.cardContent}>
      <Line data={props.chartData} options={props.chartOptions} />
    </CardContent>
  </Card>;

Delivery.propTypes = {
  classes: PropTypes.object.isRequired,
  chartData: PropTypes.object.isRequired,
  chartOptions: PropTypes.object.isRequired,
  deliveryAggregateBy: PropTypes.string.isRequired,
  handleDeliveryAggChange: PropTypes.func.isRequired
};

export default withStyles(CardStyles)(Delivery);
