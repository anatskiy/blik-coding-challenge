import React from 'react';
import PropTypes from 'prop-types';
import { Line } from 'react-chartjs-2';
import { withStyles } from 'material-ui/styles';
import Card, { CardHeader, CardContent } from 'material-ui/Card';
import CardStyles from './CardStyles';

const Throughput = ({ classes, chartData, chartOptions }) =>
  <Card>
    <CardHeader
      title={'Prozess Durchlaufs Zeit'}
      classes={{title: classes.cardTitle}}
    />
    <CardContent className={classes.cardContent + ' ' + classes.throughputCard}>
      <Line data={chartData} options={chartOptions} />
    </CardContent>
  </Card>;

Throughput.propTypes = {
  classes: PropTypes.object.isRequired,
  chartData: PropTypes.object.isRequired,
  chartOptions: PropTypes.object.isRequired
};

export default withStyles(CardStyles)(Throughput);
