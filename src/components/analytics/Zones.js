import React from 'react';
import PropTypes from 'prop-types';
import { Line } from 'react-chartjs-2';

import { withStyles } from 'material-ui/styles';
import Card, { CardHeader, CardContent } from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import { FormControl } from 'material-ui/Form';
import Select from 'material-ui/Select';
import { MenuItem } from 'material-ui/Menu';
import CardStyles from './CardStyles';

const Zones = props => {
  const {
    classes,
    zoneNames,
    selectZone,
    zone,
    zoneDescription,
    chartData,
    chartOptions
  } = props;

  return (
    <Card>
      <CardHeader
        title={'Durchschnittliche Verweildauer'}
        classes={{title: classes.cardTitle}}
        action={
          <FormControl className={classes.formControl}>
            <Select
              value={zone}
              onChange={e => selectZone(e)}
              inputProps={{ name: 'zone' }}
            >
              {zoneNames.map((name, idx) => {
                return <MenuItem value={idx} key={idx}>{name}</MenuItem>;
              })}
            </Select>
          </FormControl>
        }
      />
      <CardContent className={classes.cardContent}>
        <Line data={chartData} options={chartOptions} />
      </CardContent>
      <CardContent>
        <Typography>{zoneDescription}</Typography>
      </CardContent>
    </Card>
  );
};

Zones.propTypes = {
  classes: PropTypes.object.isRequired,
  chartData: PropTypes.object.isRequired,
  chartOptions: PropTypes.object.isRequired,
  selectZone: PropTypes.func.isRequired,
  zone: PropTypes.number.isRequired,
  zoneDescription: PropTypes.string.isRequired,
  zoneNames: PropTypes.array.isRequired
};

export default withStyles(CardStyles)(Zones);
