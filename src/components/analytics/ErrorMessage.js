import React from 'react';
import Typography from 'material-ui/Typography';

const ErrorMessage = ({ error }) =>
  <Typography>{error || 'Error.'}</Typography>;

export default ErrorMessage;
