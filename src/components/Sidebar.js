import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { withStyles } from 'material-ui/styles';
import Drawer from 'material-ui/Drawer';
import Hidden from 'material-ui/Hidden';
import Divider from 'material-ui/Divider';
import Typography from 'material-ui/Typography';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';

import { toggleSidebar } from '../actions';

const styles = theme => ({
  logo: theme.mixins.toolbar,
  logoText: {
    margin: theme.spacing.unit,
    marginLeft: theme.spacing.unit * 3,
    [theme.breakpoints.up('sm')]: {
      margin: theme.spacing.unit * 1.5,
      marginLeft: theme.spacing.unit * 3
    },
    [theme.breakpoints.down('xs')]: {
      marginLeft: theme.spacing.unit * 2
    }
  },
  drawerPaper: {
    width: theme.drawerWidth,
    [theme.breakpoints.up('md')]: {
      position: 'relative'
    }
  },
  active: {
    backgroundColor: '#bdbdbd !important'
  }
});

const mapStateToProps = state => ({
  mobileOpen: state.sidebar.mobileOpen
});

const mapDispatchToProps = dispatch => ({
  toggleSidebar: () => dispatch(toggleSidebar())
});

class Sidebar extends Component {
  isActiveRoute (path) {
    return path !== '/' && window.location.pathname.indexOf(path) > -1;
  }

  render () {
    const {
      classes,
      routes,
      logoText,
      mobileOpen,
      toggleSidebar
    } = this.props;

    const drawer = (
      <Fragment>
        <div className={classes.logo}>
          <Typography variant='display1' className={classes.logoText}>
            {logoText}
          </Typography>
        </div>
        <Divider />
        <List>
          {routes.map((obj, key) => {
            if (obj.redirect || !obj.sidebar) return null;
            return (
              <ListItem button
                component={NavLink}
                to={obj.path}
                key={key}
                className={this.isActiveRoute(obj.path) ? classes.active : ''}
              >
                <ListItemIcon><obj.icon /></ListItemIcon>
                <ListItemText primary={obj.sidebarName} />
              </ListItem>
            );
          })}
        </List>
      </Fragment>
    );

    return (
      <Fragment>
        <Hidden mdUp>
          <Drawer
            variant='temporary'
            anchor='left'
            open={mobileOpen}
            onClick={() => { toggleSidebar(); }}
            classes={{paper: classes.drawerPaper}}
            ModalProps={{keepMounted: true}}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden smDown implementation='css'>
          <Drawer open
            variant='permanent'
            classes={{paper: classes.drawerPaper}}
          >
            {drawer}
          </Drawer>
        </Hidden>
      </Fragment>
    );
  }
}

Sidebar.propTypes = {
  classes: PropTypes.object.isRequired,
  mobileOpen: PropTypes.bool.isRequired,
  toggleSidebar: PropTypes.func.isRequired
};

export default withStyles(styles)(
  connect(mapStateToProps, mapDispatchToProps)(Sidebar)
);
