import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { MuiThemeProvider, createMuiTheme, withStyles } from 'material-ui/styles';
import amber from 'material-ui/colors/amber';

import Header from './components/Header';
import Sidebar from './components/Sidebar';
import routes from './routes';

const theme = createMuiTheme({
  palette: {
    primary: amber
  },
  drawerWidth: 250
});

const styles = theme => ({
  root: {
    flexGrow: 1,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%'
  },
  toolbar: theme.mixins.toolbar,
  mainPanel: {
    flexGrow: 1
  },
  content: {
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    height: '100%'
  }
});

const switchRoutes = (
  <Switch>
    {routes.map((obj, key) => {
      if (obj.redirect) {
        return <Redirect from={obj.from} to={obj.to} key={key} />;
      } else if (obj.component !== null) {
        return <Route exact path={obj.path} component={obj.component} key={key} />;
      } else {
        return null;
      }
    })}
  </Switch>
);

const App = ({ classes }) =>
  <MuiThemeProvider theme={theme}>
    <div className={classes.root}>
      <Sidebar routes={routes} logoText={'blik'} />
      <div className={classes.mainPanel}>
        <Header />
        <main className={classes.content}>
          <div className={classes.toolbar} />
          {switchRoutes}
        </main>
      </div>
    </div>
  </MuiThemeProvider>;

export default withStyles(styles)(App);
