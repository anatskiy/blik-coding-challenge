import AnalyticsListView from 'containers/AnalyticsListView';
import AnalyticsDetailView from 'containers/AnalyticsDetailView';
import DashboardIcon from '@material-ui/icons/Dashboard';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import SettingsIcon from '@material-ui/icons/Settings';

export default [
  {
    path: '/',
    sidebar: true,
    sidebarName: 'Prozess Übersicht',
    icon: DashboardIcon,
    component: null
  },
  {
    path: '/',
    sidebar: true,
    sidebarName: 'Administration',
    icon: PermIdentityIcon,
    component: null
  },
  {
    path: '/analytics',
    sidebar: true,
    sidebarName: 'Analyse',
    icon: ShowChartIcon,
    component: AnalyticsListView
  },
  {
    path: '/analytics/:id',
    sidebar: false,
    component: AnalyticsDetailView
  },
  {
    path: '/',
    sidebar: true,
    sidebarName: 'Konfiguration',
    icon: SettingsIcon,
    component: null
  },
  { redirect: true, path: '/', to: '/analytics' }
];
