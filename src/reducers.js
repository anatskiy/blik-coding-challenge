import { combineReducers } from 'redux';
import {
  TOGGLE_SIDEBAR,
  FETCH_RESOURCE_IDS_SUCCESS,
  FETCH_RESOURCE_SUCCESS,
  FETCH_DELIVERIES_SUCCESS,
  FETCH_THROUGHPUT_SUCCESS,
  FETCH_DISTRIBUTION_SUCCESS
} from './actionTypes';

const sidebarInitialState = {
  mobileOpen: false
};

const sidebar = (state = sidebarInitialState, action) => {
  switch (action.type) {
    case TOGGLE_SIDEBAR:
      return { ...state, mobileOpen: !state.mobileOpen };
    default:
      return state;
  }
};

const analyticsInitialState = {
  ids: []
};

const analytics = (state = analyticsInitialState, action) => {
  switch (action.type) {
    case FETCH_RESOURCE_IDS_SUCCESS:
      return {...state, ids: action.payload};
    default:
      return state;
  }
};

const analyticsDetailInitialState = {
  name: '',
  numberOfSLTs: 0,
  location: {},
  zones: [],
  deliveries: [],
  throughput: [],
  distribution: []
};

const analyticsDetail = (state = analyticsDetailInitialState, action) => {
  switch (action.type) {
    case FETCH_RESOURCE_SUCCESS:
      return {
        ...state,
        name: action.payload.name,
        numberOfSLTs: action.payload.numbers,
        location: action.payload.location,
        zones: action.payload.zones
      };
    case FETCH_DELIVERIES_SUCCESS:
      return { ...state, deliveries: action.payload.deliveries };
    case FETCH_THROUGHPUT_SUCCESS:
      return { ...state, throughput: action.payload.throughput };
    case FETCH_DISTRIBUTION_SUCCESS:
      return {...state, distribution: action.payload.distribution};
    default:
      return state;
  }
};

export default combineReducers({
  sidebar,
  analytics,
  analyticsDetail
});
