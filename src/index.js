import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { Router, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import registerServiceWorker from './registerServiceWorker';

import Noty from 'noty';
import 'noty/lib/noty.css';
import 'noty/lib/themes/relax.css';

import rootReducer from './reducers';
import App from './App';
import './index.css';

const store = createStore(rootReducer);
const hist = createBrowserHistory();

Noty.overrideDefaults({
  layout: 'bottomRight',
  type: 'information',
  timeout: 2500,
  theme: 'relax'
});

ReactDOM.render(
  <Provider store={store}>
    <Router history={hist}>
      <Switch>
        <Route path='/' component={App} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('root')
);

registerServiceWorker();
