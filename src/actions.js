import {
  TOGGLE_SIDEBAR,
  FETCH_RESOURCE_IDS_SUCCESS,
  FETCH_RESOURCE_SUCCESS,
  FETCH_DELIVERIES_SUCCESS,
  FETCH_THROUGHPUT_SUCCESS,
  FETCH_DISTRIBUTION_SUCCESS
} from './actionTypes';

export const toggleSidebar = () => ({
  type: TOGGLE_SIDEBAR
});

export const fetchResourceIds = payload => ({
  type: FETCH_RESOURCE_IDS_SUCCESS,
  payload
});

export const fetchResource = payload => ({
  type: FETCH_RESOURCE_SUCCESS,
  payload
});

export const fetchThroughput = payload => ({
  type: FETCH_THROUGHPUT_SUCCESS,
  payload
});

export const fetchDeliveries = payload => ({
  type: FETCH_DELIVERIES_SUCCESS,
  payload
});

export const fetchDistribution = payload => ({
  type: FETCH_DISTRIBUTION_SUCCESS,
  payload
});
